﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    class LaptopFactory : StarDeviceFactory
    {
        public StarDevice CreateDevice(string type, string model, string brand, string processorModel, string screenModel, int id)
        {
            return new Laptop
            {
                Type = type,
                Model = model,
                Brand = brand,
                ProcessorModel = processorModel,
                ScreenModel = screenModel,
                ID = id
            };
        }
    }
}
