﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    class Program
    {
        static List<StarDevice> devices = new List<StarDevice>();
        static int nextID = 1;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Menu:");
                Console.WriteLine("1. Review of existing equipment:");
                Console.WriteLine("2. Add a new device:");
                Console.WriteLine("3. Editing existing equipment by ID:");
                Console.WriteLine("4. Removing existing equipment by ID:");
                Console.WriteLine("5. EXIT");
                Console.Write("Select the option: ");

                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        ViewDevices();
                        break;
                    case "2":
                        AddDevice();
                        break;
                    case "3":
                        EditDevice();
                        break;
                    case "4":
                        DeleteDevice();
                        break;
                    case "5":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Wrong choice. Please try again.");
                        break;
                }
            }
        }

        static void ViewDevices()
        {
            Console.WriteLine();
            Console.WriteLine("Existing equipment:");
            foreach (var device in devices)
            {
                Console.WriteLine();
                Console.WriteLine($"Type techniq: {device.Type},ID: {device.ID}, Brand: {device.Brand}, Model: {device.Model}, Processor: {device.ProcessorModel}, Screen: {device.ScreenModel}");
            }
        }

        static void AddDevice()
        {
            try
            {
                Console.Write("Type of device (for example, Smartphone): ");
                string type = Console.ReadLine();
                Console.Write("Model: ");
                string model = Console.ReadLine();
                Console.Write("Brand: ");
                string brand = Console.ReadLine();
                Console.Write("Processor model: ");
                string processorModel = Console.ReadLine();
                Console.Write("Screen model: ");
                string screenModel = Console.ReadLine();

                StarDeviceFactory factory;
                switch (type.ToLower())
                {
                    case "smartphone":
                        factory = new SmartphoneFactory();
                        break;
                    case "laptop":
                        factory = new LaptopFactory();
                        break;
                    case "tablet":
                        factory = new TabletFactory();
                        break;
                    default:
                        throw new ArgumentException("Unknown type of equipment");
                }

                devices.Add(factory.CreateDevice(type,model, brand, processorModel, screenModel, nextID++));
                Console.WriteLine("The device has been added successfully.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"ERROR: {ex.Message}");
                Console.WriteLine("Please try again.");
                AddDevice();
            }
            catch (FormatException)
            {
                Console.WriteLine("Error: The format of the data you entered is incorrect. Please try again.");
                AddDevice(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        static void EditDevice()
        {
            Console.WriteLine();
            Console.Write("Enter the ID of the device you want to edit: ");
            int id = int.Parse(Console.ReadLine());
            StarDevice device = devices.Find(d => d.ID == id);
            if (device == null)
            {
                Console.WriteLine();
                Console.WriteLine("The device with the entered ID was not found.");
                return;
            }

            Console.Write("New model: ");
            string model = Console.ReadLine();
            Console.Write("New brand: ");
            string brand = Console.ReadLine();
            Console.Write("New processor model: ");
            string processorModel = Console.ReadLine();
            Console.Write("New screen model: ");
            string screenModel = Console.ReadLine();

            device.Model = model;
            device.Brand = brand;
            device.ProcessorModel = processorModel;
            device.ScreenModel = screenModel;

            Console.WriteLine();
            Console.WriteLine("Device information has been successfully changed.");
            Console.WriteLine();
        }

        static void DeleteDevice()
        {
            Console.WriteLine();
            Console.Write("Enter the ID of the device you want to delete: ");
            int id = int.Parse(Console.ReadLine());
            StarDevice device = devices.Find(d => d.ID == id);
            if (device == null)
            {
                Console.WriteLine();
                Console.WriteLine("The device with the entered ID was not found.");
                return;
            }

            devices.Remove(device);
            Console.WriteLine();
            Console.WriteLine("The device is successfully removed.");
            Console.WriteLine();
        }
    }
}
