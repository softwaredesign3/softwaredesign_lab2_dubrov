﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    interface StarDevice
    {
        string Type { get; set; }
        string Model { get; set; }
        string Brand { get; set; }
        string ProcessorModel { get; set; }
        string ScreenModel { get; set; }
        int ID { get; set; }
    }
}
