﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    class Laptop : StarDevice
    {
        public string Type { get; set; }
        public string Model { get; set; }
        public string Brand { get; set; }
        public string ProcessorModel { get; set; }
        public string ScreenModel { get; set; }
        public int ID { get; set; }
    }
}
