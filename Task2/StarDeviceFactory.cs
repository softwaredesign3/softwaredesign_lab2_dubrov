﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2._1
{
    interface StarDeviceFactory
    {
        StarDevice CreateDevice(string type, string model, string brand, string processorModel, string screenModel, int id);
    }
}
