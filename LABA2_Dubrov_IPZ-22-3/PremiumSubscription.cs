﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LABA2_Dubrov_IPZ_22_3
{
    public class PremiumSubscription : Subscription
    {
        public PremiumSubscription()
        {
            MonthlyPrice = 20.00m;
            MinimumSubscriptionPeriod = 14;
            IncludedChannels = new List<string> { "Basic", "News", "Documentary", "Educational", "Sports", "Movies" };
        }

        public override void DisplayDetails()
        {
            Console.WriteLine("Premium Subscription Details:");
            Console.WriteLine($"Monthly Price: {MonthlyPrice}$");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} month(s)");
            Console.WriteLine($"Included Channels: {string.Join(", ", IncludedChannels)}");
        }
    }
}
