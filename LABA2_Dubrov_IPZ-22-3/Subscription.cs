﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LABA2_Dubrov_IPZ_22_3
{
    public abstract class Subscription
    {

        public decimal MonthlyPrice { get; protected set; }
        public int MinimumSubscriptionPeriod { get; protected set; }
        public List<string> IncludedChannels { get; protected set; }
        public abstract void DisplayDetails();
    }
}
