﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LABA2_Dubrov_IPZ_22_3
{
    public class EducationalSubscription : Subscription
    {
        public EducationalSubscription()
        {
            MonthlyPrice = 10.00m;
            MinimumSubscriptionPeriod = 14;
            IncludedChannels = new List<string> { "Basic", "Documentary", "Educational" };
        }

        public override void DisplayDetails()
        {
            Console.WriteLine("Educational Subscription Details:");
            Console.WriteLine($"Monthly Price: {MonthlyPrice}$");
            Console.WriteLine($"Minimum Subscription Period: {MinimumSubscriptionPeriod} month(s)");
            Console.WriteLine($"Included Channels: {string.Join(", ", IncludedChannels)}");
        }
    }
}

