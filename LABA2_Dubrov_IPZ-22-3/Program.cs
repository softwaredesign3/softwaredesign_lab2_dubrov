﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LABA2_Dubrov_IPZ_22_3
{
    class Program
    {
       
        static List<Subscription> subscriptions = new List<Subscription>();
        static decimal balance = 0;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("1. Buy a subscription");
                Console.WriteLine("2. Deposit cash");
                Console.WriteLine("3. View purchased subscriptions");
                Console.WriteLine("4. View all available subscriptions");
                Console.WriteLine("5. View cash balance and subscription term");
                Console.WriteLine("6. Exit");
                Console.WriteLine();
                Console.Write("Select the option: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        BuySubscription();
                        break;
                    case "2":
                        AddFunds();
                        break;
                    case "3":
                        DisplaySubscriptions();
                        break;
                    case "4":
                        DisplayAvailableSubscriptions();
                        break;
                    case "5":
                        DisplayBalanceAndSubscriptionStatus();
                        break;
                    case "6":
                        Console.WriteLine();
                        Console.WriteLine("Thank you for using our services and have a great day!");
                        return;
                    default:
                        Console.WriteLine();
                        Console.WriteLine("Select the correct option.");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void BuySubscription()
        {
            Console.WriteLine();
            Console.WriteLine("Choose a subscription type:");
            Console.WriteLine("1. Premium Subscription");
            Console.WriteLine("2. Educational Subscription");
            Console.WriteLine("3. Domestic Subscription");
            Console.Write("Your choice: ");
            string choice = Console.ReadLine();

            Subscription subscription = null;

            switch (choice)
            {
                case "1":
                    {
                        WebSite webSite = new WebSite();
                        subscription = webSite.CreateSubscription("premium");
                        break;
                    }
                case "2":
                    {
                        MobileApp mobileApp = new MobileApp();
                        subscription = mobileApp.CreateSubscription("educational");
                        break;
                    }
                case "3":
                    {
                        ManagerCall managerCall = new ManagerCall();
                        subscription = managerCall.CreateSubscription("domestic");
                        break;
                    }
                default:
                    Console.WriteLine();
                    Console.WriteLine("Wrong choice.");
                    return;
            }

            if (balance >= subscription.MonthlyPrice)
            {
                balance -= subscription.MonthlyPrice;
                subscriptions.Add(subscription);
                Console.WriteLine();
                Console.WriteLine("Subscription successfully purchased!");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Not enough cash to purchase a subscription.");
            }
        }

        static void AddFunds()
        {
            Console.WriteLine();
            Console.Write("Enter the amount to deposit: ");
            decimal amount;
            if (decimal.TryParse(Console.ReadLine(), out amount) && amount > 0)
            {
                balance += amount;
                Console.WriteLine();
                Console.WriteLine($"Cash has been successfully deposited to {amount}$.");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Incorrect amount.");
            }
        }

        static void DisplaySubscriptions()
        {
            if (subscriptions.Count == 0)
            {
                Console.WriteLine();
                Console.WriteLine("You have not purchased a subscription yet.");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("Purchased subscriptions:");
                foreach (var subscription in subscriptions)
                {
                    subscription.DisplayDetails();
                    Console.WriteLine();
                }
            }
        }

        static void DisplayAvailableSubscriptions()
        {
            Console.WriteLine();
            Console.WriteLine("Subscriptions are available:");
            Console.WriteLine("1. Premium Subscription");
            Console.WriteLine("2. Educational Subscription");
            Console.WriteLine("3. Domestic Subscription");
        }

        static void DisplayBalanceAndSubscriptionStatus()
        {
            Console.WriteLine();
            Console.WriteLine($"Your balance: {balance}$");
            Console.WriteLine("Status of subscriptions:");

            if (subscriptions.Count == 0)
            {
                Console.WriteLine();
                Console.WriteLine("You have not purchased a subscription yet.");
            }
            else
            {
                foreach (var subscription in subscriptions)
                {
                    DateTime expiryDate = DateTime.Now.AddMonths(subscription.MinimumSubscriptionPeriod);
                    Console.WriteLine($"Type: {subscription.GetType().Name}, Days left: {(expiryDate - DateTime.Now).Days}");
                }
            }
        }
    }
}