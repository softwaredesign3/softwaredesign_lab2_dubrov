﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class Program
    {
        static List<Character> characters = new List<Character>();
        static CharacterDirector director;
        static CharacterBuilder builder;

        static void Main(string[] args)
        {
            bool exit = false;
            do
            {
                Console.WriteLine("\nCharacter Menu");
                Console.WriteLine("1. Create Character");
                Console.WriteLine("2. Edit Character");
                Console.WriteLine("3. Delete Character");
                Console.WriteLine("4. List Characters");
                Console.WriteLine("5. Exit");
                Console.Write("Choose an option: ");
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        CreateCharacter();
                        break;
                    case "2":
                        EditCharacter();
                        break;
                    case "3":
                        DeleteCharacter();
                        break;
                    case "4":
                        ListCharacters();
                        break;
                    case "5":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option.");
                        break;
                }
            } while (!exit);
        }

        static void CreateCharacter()
        {
            Console.WriteLine("\nCharacter Creation Menu");
            Console.WriteLine("1. Create Hero");
            Console.WriteLine("2. Create Enemy");
            Console.Write("Choose an option: ");
            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    builder = new HeroBuilder();
                    director = new CharacterDirector(builder);
                    director.BuildCharacter(
                        GetName(),
                        GetAge(),
                        GetHeight(),
                        GetHairColor(),
                        GetEyeColor(),
                        GetClothing(),
                        GetInventory());
                    characters.Add(director.GetCharacter());
                    Console.WriteLine("\nHero Created:");
                    characters[characters.Count - 1].DisplayInfo();
                    break;

                case "2":
                    builder = new EnemyBuilder();
                    director = new CharacterDirector(builder);
                    director.BuildCharacter(
                        GetName(),
                        GetAge(),
                        GetHeight(),
                        GetHairColor(),
                        GetEyeColor(),
                        GetClothing(),
                        GetInventory());
                    EnemyBuilder evilBuilder = (EnemyBuilder)builder;
                    characters.Add(evilBuilder.CreateEvilCharacter().Build());
                    Console.WriteLine("\nEnemy Created:");
                    characters[characters.Count - 1].DisplayInfo();
                    break;

                default:
                    Console.WriteLine("Invalid option.");
                    break;
            }
        }

        static void EditCharacter()
        {
            ListCharacters();
            Console.Write("Enter the index of the character you want to edit: ");
            int index = int.Parse(Console.ReadLine()) - 1;
            if (index >= 0 && index < characters.Count)
            {
                Console.WriteLine("\nEditing Character:");
                characters[index].DisplayInfo();
                Console.WriteLine("\nEnter new data:");

                director = new CharacterDirector(new HeroBuilder());

                director.BuildCharacter(
                    GetName(),
                    GetAge(),
                    GetHeight(),
                    GetHairColor(),
                    GetEyeColor(),
                    GetClothing(),
                    GetInventory());
                characters[index] = director.GetCharacter();

                Console.WriteLine("\nCharacter edited successfully:");
                characters[index].DisplayInfo();
            }
            else
            {
                Console.WriteLine("Invalid character index.");
            }
        }

        static void DeleteCharacter()
        {
            ListCharacters();
            Console.Write("Enter the index of the character you want to delete: ");
            int index = int.Parse(Console.ReadLine()) - 1;
            if (index >= 0 && index < characters.Count)
            {
                characters.RemoveAt(index);
                Console.WriteLine("Character deleted successfully.");
            }
            else
            {
                Console.WriteLine("Invalid character index.");
            }
        }

        static void ListCharacters()
        {
            Console.WriteLine("\nCharacter List:");
            for (int i = 0; i < characters.Count; i++)
            {
                Console.WriteLine($"Character {i + 1}:");
                characters[i].DisplayInfo();
                Console.WriteLine();
            }
        }

        static string GetName()
        {
            Console.Write("Enter name: ");
            return Console.ReadLine();
        }

        static int GetAge()
        {
            Console.Write("Enter age: ");
            return int.Parse(Console.ReadLine());
        }

        static double GetHeight()
        {
            Console.Write("Enter height (in cm): ");
            return double.Parse(Console.ReadLine());
        }

        static string GetHairColor()
        {
            Console.Write("Enter hair color: ");
            return Console.ReadLine();
        }

        static string GetEyeColor()
        {
            Console.Write("Enter eye color: ");
            return Console.ReadLine();
        }

        static string GetClothing()
        {
            Console.Write("Enter clothing: ");
            return Console.ReadLine();
        }

        static List<string> GetInventory()
        {
            Console.Write("Enter inventory: ");
            string inventoryString = Console.ReadLine();
            return new List<string>(inventoryString.Split(','));
        }
    }
}
