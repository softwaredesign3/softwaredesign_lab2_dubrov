﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class HeroBuilder : CharacterBuilder
    {
        private Character character = new Character();

        public CharacterBuilder SetName(string name)
        {
            character.Name = name;
            return this;
        }

        public CharacterBuilder SetAge(int age)
        {
            character.Age = age;
            return this;
        }

        public CharacterBuilder SetHeight(double height)
        {
            character.Height = height;
            return this;
        }

        public CharacterBuilder SetHairColor(string hairColor)
        {
            character.HairColor = hairColor;
            return this;
        }

        public CharacterBuilder SetEyeColor(string eyeColor)
        {
            character.EyeColor = eyeColor;
            return this;
        }

        public CharacterBuilder SetClothing(string clothing)
        {
            character.Clothing = clothing;
            return this;
        }

        public CharacterBuilder SetInventory(List<string> inventory)
        {
            character.Inventory = inventory;
            return this;
        }

        public Character Build()
        {
            return character;
        }
    }
}