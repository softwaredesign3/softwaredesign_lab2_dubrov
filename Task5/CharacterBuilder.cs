﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    interface CharacterBuilder
    {
        CharacterBuilder SetName(string name);
        CharacterBuilder SetAge(int age);
        CharacterBuilder SetHeight(double height);
        CharacterBuilder SetHairColor(string hairColor);
        CharacterBuilder SetEyeColor(string eyeColor);
        CharacterBuilder SetClothing(string clothing);
        CharacterBuilder SetInventory(List<string> inventory);
        Character Build();
    }
}
