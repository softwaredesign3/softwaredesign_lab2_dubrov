﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task5
{
    class CharacterDirector
    {
        private CharacterBuilder characterBuilder;

        public CharacterDirector(CharacterBuilder builder)
        {
            characterBuilder = builder;
        }

        public void BuildCharacter(string name, int age, double height, string hairColor, string eyeColor, string clothing, List<string> inventory)
        {
            characterBuilder.SetName(name)
                            .SetAge(age)
                            .SetHeight(height)
                            .SetHairColor(hairColor)
                            .SetEyeColor(eyeColor)
                            .SetClothing(clothing)
                            .SetInventory(inventory);
        }

        public Character GetCharacter()
        {
            return characterBuilder.Build();
        }
    }
}
