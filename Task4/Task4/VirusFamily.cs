﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class VirusFamily
    {
        static List<Virus> familyViruses = new List<Virus>();

        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("\nMenu:");
                Console.WriteLine("1. Add Virus");
                Console.WriteLine("2. Remove Virus");
                Console.WriteLine("3. View All Family Viruses");
                Console.WriteLine("4. Clone Virus");
                Console.WriteLine("5. Exit");
                Console.Write("Choose an option: ");
                string option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        AddVirus();
                        break;
                    case "2":
                        RemoveVirus();
                        break;
                    case "3":
                        ViewAllFamilyViruses();
                        break;
                    case "4":
                        CloneVirus();
                        break;
                    case "5":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option. Please choose between 1 and 5.");
                        break;
                }
            }
        }

        static void AddVirus()
        {
            Console.Write("Enter the name of the virus: ");
            string name = Console.ReadLine();
            Console.Write("Enter the type of the virus: ");
            string type = Console.ReadLine();
            Console.Write("Enter the age of the virus: ");
            int age = int.Parse(Console.ReadLine());

            if (familyViruses.Count == 0)
            {
                Virus newVirus = new Virus(name, type, age);
                familyViruses.Add(newVirus);
                Console.WriteLine("Virus successfully added to the family!");
            }
            else
            {
                Console.Write("Enter the parent virus name: ");
                string parentName = Console.ReadLine();

                Virus parentVirus = familyViruses.Find(v => v.Name == parentName);
                if (parentVirus != null)
                {
                    Virus newVirus = new Virus(name, type, age, parentVirus);
                    familyViruses.Add(newVirus);
                    Console.WriteLine("Virus successfully added to the family!");
                }
                else
                {
                    Console.WriteLine($"Parent virus with the name {parentName} not found in the family!");
                }
            }
        }

        static void RemoveVirus()
        {
            Console.Write("Enter the name of the virus you want to remove: ");
            string name = Console.ReadLine();

            Virus virusToRemove = familyViruses.Find(v => v.Name == name);
            if (virusToRemove != null)
            {
                familyViruses.Remove(virusToRemove);
                Console.WriteLine($"Virus {name} successfully removed from the family!");
            }
            else
            {
                Console.WriteLine($"Virus with the name {name} not found in the family!");
            }
        }

        static void ViewAllFamilyViruses()
        {
            Console.WriteLine("\nAll family viruses:");
            foreach (var virus in familyViruses)
            {
                Console.WriteLine($"Name: {virus.Name}, Type: {virus.Type}, Age: {virus.Age}");
            }
        }

        static void CloneVirus()
        {
            Console.Write("Enter the name of the virus you want to clone: ");
            string name = Console.ReadLine();

            Virus virusToClone = familyViruses.Find(v => v.Name == name);
            if (virusToClone != null)
            {
                Virus clonedVirus = (Virus)virusToClone.Clone();
                familyViruses.Add(clonedVirus);
                Console.WriteLine($"Virus {name} successfully cloned!");
            }
            else
            {
                Console.WriteLine($"Virus with the name {name} not found in the family!");
            }
        }
    }

    class Virus : ICloneable
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public int Age { get; set; }
        public List<Virus> Children { get; set; }

        public Virus(string name, string type, int age, Virus parent = null)
        {
            Name = name;
            Type = type;
            Age = age;
            Children = new List<Virus>();

            if (parent != null)
            {
                parent.Children.Add(this);
            }
        }

        public object Clone()
        {
            Virus clone = (Virus)this.MemberwiseClone();
            clone.Children = new List<Virus>();

            foreach (var child in Children)
            {
                clone.Children.Add((Virus)child.Clone());
            }

            return clone;
        }
    }
}
