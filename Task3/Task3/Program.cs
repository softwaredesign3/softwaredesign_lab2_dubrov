﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Authenticator authentification1 = Authenticator.GetInstance();
            Authenticator authentification2 = Authenticator.GetInstance();

            Console.WriteLine($"authentification1 == authentification2: {authentification1 == authentification2}");
            authentification1.Authenticate("userAdmin", "passwordadmin");
        }

    }
}
